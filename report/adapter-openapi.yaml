openapi: 3.0.0
info:
  title: puppet
  description: Puppet API documentation
  contact: {}
  version: '3'
servers:
- url: https://{defaultHost}
  variables:
    defaultHost:
      default: www.example.com
paths:
  /catalog/{nodename}:
    get:
      tags:
      - Catalog
      summary: getCatalogNodename
      description: Retrieve a catalog.
      operationId: getCatalogNodename
      parameters:
      - name: nodename
        in: path
        description: Node name
        required: true
        style: simple
        schema:
          type: string
      - name: environment
        in: query
        description: the environment name.
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: facts_format
        in: query
        description: must be 'application/json' or 'pson'.
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: facts
        in: query
        description: serialized JSON or PSON of the facts hash. Since facts can contain &, which is also the HTTP query parameter delimiter, facts are doubly-escaped.
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: transaction_uuid
        in: query
        description: a transaction uuid identifying the entire transaction.
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: static_catalog
        in: query
        description: a boolean requesting a static catalog if available; should always be true.
        style: form
        explode: true
        schema:
          type: boolean
      - name: checksum_type
        in: query
        description: a dot-separated list of checksum types supported by the agent.
        style: form
        explode: true
        schema:
          type: string
      - name: configured_environment
        in: query
        description: the environment configured on the client.
        style: form
        explode: true
        schema:
          type: string
      - name: job_id
        in: query
        description: which orchestration job triggered this catalog request.
        style: form
        explode: true
        schema:
          type: string
      responses:
        '200':
          description: Success
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RootTypeforCatalog'
      deprecated: false
    post:
      tags:
      - Catalog
      summary: postCatalogNodename
      description: Retrieve a catalog.
      operationId: postCatalogNodename
      parameters:
      - name: nodename
        in: path
        description: Node name
        required: true
        style: simple
        schema:
          type: string
      requestBody:
        description: ''
        content:
          application/json:
            schema:
              allOf:
              - $ref: '#/components/schemas/RootTypeforCatalogRequestBody'
              - example:
                  environment: string
                  facts_format: string
                  facts: string
                  transaction_uuid: string
                  static_catalog: boolean
                  checksum_type: string
                  configured_environment: string
                  job_id: string
            example:
              environment: string
              facts_format: string
              facts: string
              transaction_uuid: string
              static_catalog: boolean
              checksum_type: string
              configured_environment: string
              job_id: string
        required: true
      responses:
        '200':
          description: Success
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RootTypeforCatalog'
      deprecated: false
  /node/{certname}:
    get:
      tags:
      - Node
      summary: getNodeCertname
      description: Retrieve data for a node.
      operationId: getNodeCertname
      parameters:
      - name: certname
        in: path
        description: cert name
        required: true
        style: simple
        schema:
          type: string
      - name: environment
        in: query
        description: the environment name.
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: transaction_uuid
        in: query
        description: a transaction uuid identifying the entire transaction.
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: configured_environment
        in: query
        description: the environment configured on the client.
        style: form
        explode: true
        schema:
          type: string
      responses:
        '200':
          description: Success
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RootTypeforNode'
      deprecated: false
  /environments:
    get:
      tags:
      - Environments
      summary: getEnvironments
      description: Get the list of known environments.
      operationId: getEnvironments
      parameters: []
      responses:
        '200':
          description: Success
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RootTypeforEnvironment'
      deprecated: false
  /report/{nodename}:
    put:
      tags:
      - Report
      summary: putReportNodename
      description: Allows clients to send reports to the master
      operationId: putReportNodename
      parameters:
      - name: nodename
        in: path
        description: Node name
        required: true
        style: simple
        schema:
          type: integer
          format: int32
      - name: environment
        in: query
        description: environment name
        required: true
        style: form
        explode: true
        schema:
          type: string
      requestBody:
        description: ''
        content:
          application/json:
            schema:
              allOf:
              - $ref: '#/components/schemas/RootTypeforReportRequestBody'
              - example: {}
            example: {}
        required: true
      responses:
        '200':
          description: Success
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RootTypeforReport'
      deprecated: false
  /status/{name}:
    get:
      tags:
      - Status
      summary: getStatusName
      description: Provides information about a running master.
      operationId: getStatusName
      parameters:
      - name: name
        in: path
        description: Name of a running master
        required: true
        style: simple
        schema:
          type: string
      - name: environment
        in: query
        description: environment name
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        '200':
          description: Success
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RootTypeforStatus'
      deprecated: false
  /file_content/{mount_point}/{name}:
    get:
      tags:
      - FileContent
      summary: getFilecontentMountpointName
      description: Retrieve the contents of the specified file.
      operationId: getFilecontentMountpointName
      parameters:
      - name: mount_point
        in: path
        description: mount point
        required: true
        style: simple
        schema:
          type: string
      - name: name
        in: path
        description: the path to the file within the mount_point that is requested.
        required: true
        style: simple
        schema:
          type: string
      responses:
        '200':
          description: Success
          headers: {}
          content: {}
      deprecated: false
  /file_metadata/{mount}/{path}:
    get:
      tags:
      - FileMetadata
      summary: getFilemetadataMountPath
      description: Returns select metadata for a single file or many files.
      operationId: getFilemetadataMountPath
      parameters:
      - name: environment
        in: query
        description: environment name
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: mount
        in: path
        description: mount name
        required: true
        style: simple
        schema:
          type: string
      - name: path
        in: path
        description: path to file
        required: true
        style: simple
        schema:
          type: string
      - name: links
        in: query
        description: either manage (default) or follow.
        style: form
        explode: true
        schema:
          type: string
      - name: checksum_type
        in: query
        description: the checksum type to calculate the checksum value for the result metadata.
        style: form
        explode: true
        schema:
          type: string
      - name: source_permissions
        in: query
        description: whether (and how) Puppet should copy owner, group, and mode permissions.
        style: form
        explode: true
        schema:
          type: string
      responses:
        '200':
          description: Success
          headers: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RootTypeforFileMetadata'
      deprecated: false
  /file_bucket_file/md5/{checksum}:
    get:
      tags:
      - FileBucketfile
      summary: getFilebucketfileMd5Checksum
      description: Retrieve the contents of a file.
      operationId: getFilebucketfileMd5Checksum
      parameters:
      - name: checksum
        in: path
        description: checksum md5 value
        required: true
        style: simple
        schema:
          type: string
      - name: environment
        in: query
        description: environment name
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        '200':
          description: Success
          headers: {}
          content: {}
      deprecated: false
    put:
      tags:
      - FileBucketfile
      summary: putFilebucketfileMd5Checksum
      description: Save a file to the filebucket.
      operationId: putFilebucketfileMd5Checksum
      parameters:
      - name: checksum
        in: path
        description: Checksum md5 value
        required: true
        style: simple
        schema:
          type: string
      - name: environment
        in: query
        description: environment name
        required: true
        style: form
        explode: true
        schema:
          type: string
      requestBody:
        description: ''
        content:
          text/plain:
            schema:
              type: object
              example: {}
            example: {}
        required: true
      responses:
        '200':
          description: Success
          headers: {}
          content: {}
      deprecated: false
  /file_bucket_file/md5/{checksum}/{originalPath}:
    get:
      tags:
      - FileBucketfile
      summary: getFilebucketfileMd5ChecksumOriginalPath
      description: Retrieve the contents of a file.
      operationId: getFilebucketfileMd5ChecksumOriginalPath
      parameters:
      - name: checksum
        in: path
        description: checksum md5 value
        required: true
        style: simple
        schema:
          type: string
      - name: originalPath
        in: path
        description: the full absolute path of the file on the client system.
        required: true
        style: simple
        schema:
          type: string
      - name: environment
        in: query
        description: environment name
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        '200':
          description: Success
          headers: {}
          content: {}
      deprecated: false
    put:
      tags:
      - FileBucketfile
      summary: putFilebucketfileMd5ChecksumOriginalPath
      description: Save a file to the filebucket.
      operationId: putFilebucketfileMd5ChecksumOriginalPath
      parameters:
      - name: checksum
        in: path
        description: Checksum md5 value
        required: true
        style: simple
        schema:
          type: string
      - name: originalPath
        in: path
        description: the full absolute path of the file on the client system.
        required: true
        style: simple
        schema:
          type: string
      - name: environment
        in: query
        description: environment name
        required: true
        style: form
        explode: true
        schema:
          type: string
      requestBody:
        description: ''
        content:
          text/plain:
            schema:
              type: object
              example: {}
            example: {}
        required: true
      responses:
        '200':
          description: Success
          headers: {}
          content: {}
      deprecated: false
components:
  schemas:
    RootTypeforCatalog:
      title: RootTypeforCatalog
      type: object
      properties:
        tags:
          type: array
          items:
            type: string
          description: ''
        name:
          type: string
        version:
          type: integer
          format: int32
        code_id:
          type: string
        catalog_uuid:
          type: string
        catalog_format:
          type: integer
          format: int32
        environment:
          type: string
        resources:
          type: array
          items:
            $ref: '#/components/schemas/Resource'
          description: ''
        edges:
          type: array
          items:
            $ref: '#/components/schemas/Edge'
          description: ''
        classes:
          type: array
          items:
            type: string
          description: ''
      example:
        tags:
        - settings
        - multi_param_class
        - class
        name: elmo.mydomain.com
        version: 1377473054
        code_id: 'null'
        catalog_uuid: 827a74c8-cf98-44da-9ff7-18c5e4bee41e
        catalog_format: 1
        environment: production
        resources:
        - type: Stage
          title: main
          tags:
          - stage
          exported: false
          parameters:
            name: main
        - type: Class
          title: Settings
          tags:
          - class
          - settings
          exported: false
        - type: Class
          title: main
          tags:
          - class
          exported: false
          parameters:
            name: main
        - type: Class
          title: Multi_param_class
          tags:
          - class
          - multi_param_class
          line: 10
          exported: false
          parameters:
            one: hello
            two: world
        - type: Notify
          title: foo
          tags:
          - notify
          - foo
          - class
          - multi_param_class
          line: 4
          exported: false
          parameters:
            message: One is hello, two is world
        edges:
        - source: Stage[main]
          target: Class[Settings]
        - source: Stage[main]
          target: Class[main]
        - source: Stage[main]
          target: Class[Multi_param_class]
        - source: Class[Multi_param_class]
          target: Notify[foo]
        classes:
        - settings
        - multi_param_class
    RootTypeforNode:
      title: RootTypeforNode
      type: object
      properties:
        name:
          type: string
        parameters:
          $ref: '#/components/schemas/Parameters1'
        environment:
          type: string
      example:
        name: thinky.corp.puppetlabs.net
        parameters:
          architecture: amd64
          kernel: Linux
          blockdevices: sda,sr0
          clientversion: 3.3.1
          clientnoop: 'false'
          environment: production
        environment: production
    RootTypeforEnvironment:
      title: RootTypeforEnvironment
      type: object
      properties:
        search_paths:
          type: array
          items:
            type: string
          description: ''
        environments:
          $ref: '#/components/schemas/Environments'
      example:
        search_paths:
        - /etc/puppetlabs/code/environments
        environments:
          production:
            settings:
              modulepath:
              - /etc/puppetlabs/code/environments/production/modules
              - /etc/puppetlabs/code/environments/development/modules
              manifest:
              - /etc/puppetlabs/code/environments/production/manifests
              environment_timeout: 180
              config_version: /version/of/config
    RootTypeforReport:
      title: RootTypeforReport
      type: object
      properties:
        host:
          type: string
        time:
          type: string
        configuration_version:
          type: integer
          format: int32
        transaction_uuid:
          type: string
        code_id:
          type: string
        job_id:
          type: string
        catalog_uuid:
          type: string
        catalog_format:
          type: integer
          format: int32
        report_format:
          type: integer
          format: int32
        puppet_version:
          type: string
        status:
          type: string
        transaction_completed:
          type: boolean
        noop:
          type: boolean
        noop_pending:
          type: boolean
        environment:
          type: string
        logs:
          type: array
          items:
            $ref: '#/components/schemas/Log'
          description: ''
        metrics:
          $ref: '#/components/schemas/Metrics'
        resource_statuses:
          $ref: '#/components/schemas/ResourceStatuses'
        cached_catalog_status:
          type: string
    RootTypeforStatus:
      title: RootTypeforStatus
      type: object
      properties:
        is_alive:
          type: boolean
        version:
          type: string
      example:
        is_alive: true
        version: 3.3.2
    RootTypeforFileMetadata:
      title: RootTypeforFileMetadata
      type: object
      properties:
        checksum:
          $ref: '#/components/schemas/Checksum'
        destination:
          type: string
        group:
          type: integer
          format: int32
        links:
          type: string
        mode:
          type: integer
          format: int32
        owner:
          type: integer
          format: int32
        path:
          type: string
        relative_path:
          type: string
        type:
          type: string
      example:
        checksum:
          type: md5
          value: '{md5}d0a10f45491acc8743bc5a82b228f89e'
        destination: 'null'
        group: 20
        links: manage
        mode: 420
        owner: 501
        path: /etc/puppetlabs/code/modules/example/files/just_a_file.txt
        relative_path: 'null'
        type: string
        format: file
    RootTypeforReportRequestBody:
      title: RootTypeforReportRequestBody
      type: object
      properties:
        environment:
          type: string
      example: {}
    RootTypeforCatalogRequestBody:
      title: RootTypeforCatalogRequestBody
      type: object
      properties:
        facts_format:
          type: string
        facts:
          type: string
        transaction_uuid:
          type: string
        static_catalog:
          type: string
        checksum_type:
          type: string
        configured_environment:
          type: string
        job_id:
          type: string
        environment:
          type: string
      example:
        environment: string
        facts_format: string
        facts: string
        transaction_uuid: string
        static_catalog: boolean
        checksum_type: string
        configured_environment: string
        job_id: string
    Changes:
      title: Changes
      type: object
      properties:
        name:
          type: string
        label:
          type: string
        values:
          type: array
          items:
            type: array
            items:
              type: string
    Checksum:
      title: Checksum
      type: object
      properties:
        type:
          type: string
        value:
          type: string
    Edge:
      title: Edge
      type: object
      properties:
        source:
          type: string
        target:
          type: string
    Environments:
      title: Environments
      type: object
      properties:
        production:
          $ref: '#/components/schemas/Production'
    Events:
      title: Events
      type: object
      properties:
        name:
          type: string
        label:
          type: string
        values:
          type: array
          items:
            type: array
            items:
              type: string
    Log:
      title: Log
      type: object
      properties:
        level:
          type: string
        message:
          type: string
        source:
          type: string
        tags:
          type: array
          items:
            type: string
          description: ''
        time:
          type: string
        file:
          type: string
        line:
          type: string
    Metrics:
      title: Metrics
      type: object
      properties:
        resources:
          $ref: '#/components/schemas/Resources'
        time:
          $ref: '#/components/schemas/Time'
        changes:
          $ref: '#/components/schemas/Changes'
        events:
          $ref: '#/components/schemas/Events'
    NotifyAResource:
      title: NotifyAResource
      type: object
      properties:
        title:
          type: string
        file:
          type: string
        line:
          type: string
        resource:
          type: string
        resource_type:
          type: string
        provider_used:
          type: string
        containment_path:
          type: array
          items:
            type: string
          description: ''
        evaluation_time:
          type: string
        tags:
          type: array
          items:
            type: string
          description: ''
        time:
          type: string
        failed:
          type: boolean
        changed:
          type: boolean
        out_of_sync:
          type: boolean
        skipped:
          type: boolean
        change_count:
          type: integer
          format: int32
        out_of_sync_count:
          type: integer
          format: int32
        events:
          type: array
          items:
            type: object
          description: ''
    Parameters:
      title: Parameters
      type: object
      properties:
        name:
          type: string
    Parameters1:
      title: Parameters1
      type: object
      properties:
        architecture:
          type: string
        kernel:
          type: string
        blockdevices:
          type: string
        clientversion:
          type: string
        clientnoop:
          type: string
        environment:
          type: string
    Production:
      title: Production
      type: object
      properties:
        settings:
          $ref: '#/components/schemas/Settings'
    Resource:
      title: Resource
      type: object
      properties:
        type:
          type: string
        title:
          type: string
        tags:
          type: array
          items:
            type: string
          description: ''
        exported:
          type: boolean
        parameters:
          $ref: '#/components/schemas/Parameters'
    Resources:
      title: Resources
      type: object
      properties:
        name:
          type: string
        label:
          type: string
        values:
          type: array
          items:
            type: array
            items:
              type: string
    ResourceStatuses:
      title: ResourceStatuses
      type: object
      properties:
        Notify[a resource]:
          $ref: '#/components/schemas/NotifyAResource'
    Settings:
      title: Settings
      type: object
      properties:
        modulepath:
          type: array
          items:
            type: string
          description: ''
        manifest:
          type: array
          items:
            type: string
          description: ''
        environment_timeout:
          type: integer
          format: int32
        config_version:
          type: string
    Time:
      title: Time
      type: object
      properties:
        name:
          type: string
        label:
          type: string
        values:
          type: array
          items:
            type: array
            items:
              type: string
tags:
- name: Catalog
  description: ''
- name: Node
  description: ''
- name: Environments
  description: ''
- name: Report
  description: ''
- name: Status
  description: ''
- name: FileContent
  description: ''
- name: FileMetadata
  description: ''
- name: FileBucketfile
  description: ''
